---
name: BuzzFeed
tier: gold
site_url: https://www.buzzfeed.com/about/jobs
logo: buzzfeed.png
---
BuzzFeed is the leading media company for the social age, intensely focused on delivering
high-quality original reporting, insight, and viral content across a rapidly expanding array of
subject areas. Our technology powers the social distribution of content, detects what is trending on
the web, and connects people in realtime with the hottest content of the moment. Our site is rapidly
growing and reaches more than 200 million monthly unique visitors.

BuzzFeed is at the forefront of a major shift in the advertising industry away from traditional
banner ads towards "social advertising" that engages consumers, inspires sharing, and produces
social lift, or "earned media."

BuzzFeed's industry-leading technology and unique social distribution engine is used by top brands
such as GE, Virgin Mobile, and Geico to go beyond banners, turbo charging their social advertising
initiatives.

Jonah Peretti, founder & CEO of BuzzFeed, previously co-founded the Huffington Post. Ben Smith is
its Editor-in-Chief and Kenneth Lerer is the Executive Chairman.
