---
name: Bloomberg
tier: gold
site_url: https://bit.ly/Python_Bloomberg
logo: bloomberg.png
---
Today, Bloomberg’s unrivaled technology – software, hardware, and network –
continues to help drive the world’s financial markets. Nearly 5,000
technologists define, architect, build, and deploy complete systems to fulfill
the needs of leading financial market participants globally. This includes
communications platforms, real-time data, analytics, trading solutions, news and
other critical information.
