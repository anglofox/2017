---
title: Talk Schedule
body_class: schedule
---
{% assign slotted_talks = site.talks | where_exp: 'item', 'item.slot' %}
{% if slotted_talks.size > 0 %}

{% assign number_of_rooms = site.data.event.venue.rooms.size %}

{% assign days = slotted_talks | slotted_schedule: site.data.event.venue.rooms %}
{% assign days = days | sort %}
{% for day in days %}
  <h3>{{ day[0] | date: '%B %d, %Y' }}</h3>
  <div class="table-responsive">
    <table class="schedule">
        <thead>
        <tr>
            <th>&nbsp;</th>
            {% for room in site.data.event.venue.rooms %}
            <th>{{ room }}</th>
            {% endfor %}
        </tr>
        </thead>
        <tbody>
        {% assign slots = day[1] | sort %}
        {% for slot in slots %}
        <tr>
            <td class="time">{{ slot[0] | date: '%I:%M %p' }}</td>
            {% assign number_of_talks = slot[1].size %}
            {% assign colspan = number_of_rooms | divided_by: number_of_talks %}
            {% for talk in slot[1] %}
            <td class="talk {{ talk.type }}" colspan="{{ colspan }}">
                {% if talk.speakers.size > 0 %}
                <a href="{{ talk.url }}" class="title">
                    {% if talk.type == 'keynote' %}Keynote:{% endif %}
                    {{ talk.title }}
                </a>
                {{ talk.speakers | join: ', ' }}
                {% else %}
                {{ talk.title }}
                {% endif %}
            </td>
            {% endfor %}
        </tr>
        {% endfor %}
        </tbody>
    </table>
  </div>
{% endfor %}

{% endif %}
