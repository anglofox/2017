---
name: Suby Raman
talks:
- Make mind-bending, interactive 3D projects with TouchDesigner and Python
---

Suby Raman is a multimedia artist and full-stack software engineer, working for Duo Security out of Ann Arbor, Michigan. He has a doctorate in classical music composition and is constantly looking for ways to bend Python into making art and life more interesting. His current artistic work involves incorporating motion capture in interactive audiovisual experiences that invite people to dance, and even rethink how they engage with their body's movement. 

He hopes to one day create great art with data generated from his cat.