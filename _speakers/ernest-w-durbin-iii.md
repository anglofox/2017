---
name: Ernest W. Durbin III
talks:
- 'Running Vintage Software: PyPI''s Aging Codebase.'
---

Ernest is a contributor to the Infrastructure Team for The Python Software Foundation, Organizer for the Cleveland Python meetup, and upcoming Chair for PyCon US in Cleveland Ohio 2018 and 2019.