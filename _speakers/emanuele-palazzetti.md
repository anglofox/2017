---
name: Emanuele Palazzetti
talks:
- Context propagation for Python concurrency models
---

Software Engineer and Open Source lover, Emanuele works at Datadog in the Application Performance Monitoring team. While playing with high scalable systems, he improves and simplifies how a distributed system is traced across different languages. Without losing correctness, he likes writing efficient, readable and maintainable Python and Go code because "elegance is not optional."