---
abstract: Ever wonder how to be more visually accessible to those around you? American
  Sign Language (ASL) is the main form of communication for many deaf Americans, yet
  it is not widely known in the hearing community. Learn how to convert your speech
  to text and then to sign to be more inclusive!
duration: 40
level: Beginner
speakers:
- Heather Shapiro
title: There's a sign for that!
type: talk
---

Ever wonder how to be more visually accessible to those around you? American Sign Language (ASL) is the main form of communication for many deaf Americans, yet it is not widely known in the hearing community. Learn how to convert your speech to text and then to sign to be more inclusive!

This talk will explain how to use Speech to Text APIs in Python in order to record audio, convert it to text, and match it to a similar gif to show the phrase!