---
abstract: The purpose of this talk is explain the BitTorrent protocol and relevant
  Asyncio API needed to create a BitTorrent client that can actually download a ".torrent"
  file. The BitTorrent application will be built and presented as a set of steps to
  build a program that can download a file.
duration: 25
level: Beginner
speakers:
- Ahmed Abdalla
title: Creating a Bittorrent Client using Asyncio
type: talk
---

The purpose of this talk is explain the BitTorrent protocol and relevant Asyncio API used to create a BitTorrent client that can actually download a ".torrent" file. The BitTorrent application will be built and presented as a set of steps (code snippets, i.e. coroutines) that implement various parts of the protocol and build up a final program that can download a file. Talk will cover,

* Parsing a .torrent file
* Connecting to a tracker
* Establishing concurrent peer network connections using Asyncio
* Torrent download strategy
* Relevant Asyncio/event loop concepts where necessary

Attendees need not know Asyncio and will learn the relevant functions needed to accomplish the task. No prior knowledge of the BitTorrent protocol is necessary either.