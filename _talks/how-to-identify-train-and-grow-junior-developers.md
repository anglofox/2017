---
abstract: If your company is smaller than Facebook, you know that finding experienced
  developers is both hard and expensive. At the same time, many talented people fall
  through the cracks of corporate recruiting. In this talk, you'll learn how to find
  people whose skills will grow along with your company.
duration: 25
level: All
speakers:
- John Healy
title: How to Identify, Train and Grow Junior Developers
type: talk
---

One of the biggest problems facing any growing software-based company is finding the right engineers to grow the team. Often, engineering leadership responds to this need by engaging a variety of outside resources, including recruiters or internal HR departments. Unfortunately, these resources tend to be ill-equipped to find the right talent, instead taking a one-size-fits-all approach that leads the growing company to compete over the same limited and expensive resources that big companies look for.
In this talk, you'll learn how to find the many great individuals passed over by this process, many of whom are likely better fits for your business than even the best on-paper candidate. You'll learn how to help these people become great contributors to your team, often more quickly than expected. Finally, you'll learn about some pitfalls to avoid as a company in order to get the best out of your employees.
If you're new to Python or software development, this talk is also for you! You'll learn how to get the most out of your education, whether its self study, college, or a bootcamp. You'll learn how to identify small and growing teams that are worth your time and energy. Most importantly, you'll learn that breaking in to the industry takes work, but its not as hard as you think it is!