---
abstract: "In 1979,  Daniel Kahneman and Amos Tversky coined the term \u201CPlanning\
  \ Fallacy\u201D. When you look at the software development you can see the same\
  \ phenomena. \nIn this talk, Amin explains the psychology of estimation. He walks\
  \ us through the red flags that we can look for to spot Planning Fallacy."
duration: 25
level: All
speakers:
- Amin Yazdani
title: 'Psychology of Estimation: Faster is not always better'
type: talk
---

In 1979,  Daniel Kahneman and Amos Tversky coined the term “Planning Fallacy” to describe plans and forecasts that are unrealistically close to best-case scenarios. These plans can be improved by consulting the statistics of similar cases. 

When you look at the software development and projects done using Agile methodologies, you can see the same phenomena. Projects are estimated too optimistically at start. We are usually too focused on best-case scenarios. Although our experience shows that statistically projects are done closer to their worst-case scenario, we stay optimistic. When issues arise, we blame the odd task that took more than expected, or the unexpected technical difficulty that took a few extra weeks or months. But we should have known better, shouldn’t we have?

In this talk, Amin explains the psychology of estimation and the reasons behind our behaviour. He walks us through the red flags that we can look for to spot Planning Fallacy and describes solutions for estimating more accurately.