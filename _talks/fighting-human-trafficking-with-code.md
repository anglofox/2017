---
abstract: Human trafficking remains a massive problem even today. Traffickers rely
  on technology to operate, and programmers are starting to use technology to fight
  back. Learn how software and data analytics are empowering human trafficking investigators
  and what a Python programmer can do to join the fight.
duration: 25
level: All
speakers:
- Alexandra Wilde
title: Fighting Human Trafficking with Code
type: talk
---

Human Trafficking remains a massive problem in the United States. Traffickers develop sophisticated networks to support their businesses, and rely on technology to do it. Coders have the power to fight this, by empowering investigators with more sophisticated technological tools of their own. This talk discusses Python tools and analytics developed for the Human Trafficking Response Unit at the Manhattan DA's office, and what a Python programmer can do, at any experience level, to contribute to the fight.